import Adafruit_BME280
import wiringpi as wp

def readData():
    bme280 = Adafruit_BME280.BME280()
    temperature = bme280.read_temperature()
    humidity = bme280.read_humidity()
    pressure = bme280.read_pressure()/100

    return (temperature,humidity,pressure)

