import sys,os
sys.path.append(os.pardir)
import readBME280
from slackbot.bot import listen_to


@listen_to('env')
def listen_func(message):
    temp,humid,press = readBME280.readData()
    temp = str(round(temp))
    humid = str(round(humid))
    press = str(round(press))
    message.send('現在の気温は'+temp+'度、湿度は'+humid+'%、気圧は'+press+'hPaです。')
